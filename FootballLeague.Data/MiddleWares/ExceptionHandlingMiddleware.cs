﻿
using System.Net;
using System.Threading.Tasks;
using FootballLeague.Data.CustomExceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace FootballLeague.Infrastructure.Middlewares
{
    public class ExceptionHandlingMiddleware
    {
        private readonly RequestDelegate _next;
        private ILogger _logger;

        public ExceptionHandlingMiddleware(RequestDelegate next, ILogger<ExceptionHandlingMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (EntityAlreadyExistsException exception)
            {
                context.Response.ContentType = "application/json";
                context.Response.StatusCode = (int)HttpStatusCode.BadRequest;

                await context.Response.WriteAsync(exception.Message);
                _logger.LogError(exception.Message);
            }
            catch (EntityNotFoundException exception)
            {
                context.Response.ContentType = "application/json";
                context.Response.StatusCode = (int)HttpStatusCode.NotFound;

                await context.Response.WriteAsync(exception.Message);
                _logger.LogError(exception.Message);
            }
            catch (MatchIsPlayedException exception)
            {
                context.Response.ContentType = "application/json";
                context.Response.StatusCode = (int)HttpStatusCode.BadRequest;

                await context.Response.WriteAsync(exception.Message);
                _logger.LogError(exception.Message);
            }
            catch (System.Exception)
            {
                context.Response.ContentType = "application/json";
                context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

                await context.Response.WriteAsync("Something went wrong!");
                _logger.LogError("Something went wrong!");
            }            
        }
    }
}
