﻿
using FootballLeague.Infrastructure.Middlewares;
using Microsoft.AspNetCore.Builder;

namespace FootballLeague.Infrastructure.Extensions
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder ExceptionHandler(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ExceptionHandlingMiddleware>();
        }
    }
}
