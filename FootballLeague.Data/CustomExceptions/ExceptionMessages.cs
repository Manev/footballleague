﻿
namespace FootballLeague.Data.CustomExceptions
{
    public static class ExceptionMessages
    {
        public static string LeagueNotFound = "This league does not exist.";

        public static string LeagueAlreadyExists = "There is already a league with that name.";

        public static string MatchAlreadyPlayed = "This match has already been played.";

        public static string MatchNotFound = "This match does not exist.";

        public static string GenericNotFoundMessage = "One or more entities not found";

        public static string TeamNotInLeague = "There is no such team in this league";

        public static string TeamAlreadyExists = "This team already exists";

        public static string TeamDoesNotExist = "This team does not exist";
    }
}
