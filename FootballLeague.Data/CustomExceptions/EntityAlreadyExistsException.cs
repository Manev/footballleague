﻿using System;

namespace FootballLeague.Data.CustomExceptions
{
    public class EntityAlreadyExistsException : Exception
    {
        public EntityAlreadyExistsException(string message)
            : base(message)
        {
        }
    }
}
