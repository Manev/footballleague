﻿using System;

namespace FootballLeague.Data.CustomExceptions
{
    public class MatchIsPlayedException : Exception
    {
        public MatchIsPlayedException(string message)
            : base(message)
        {
        }
    }
}
