﻿using System.Collections.Generic;

namespace FootballLeague.Data.Models
{
    public class League
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public IEnumerable<Match> Matches { get; set; }

        public IEnumerable<Team> Teams { get; set; }
    }
}
