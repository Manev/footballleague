﻿
namespace FootballLeague.Data.Models.ResponseModels
{
    public class MatchResponseModel
    {
        public string HomeTeamName { get; set; }

        public string AwayTeamName { get; set; }

        public string WinnerName { get; set; }

        public string LeagueName { get; set; }

        public string Result { get; set; }
    }
}
