﻿
namespace FootballLeague.Data.Models.ResponseModels
{
    public class TeamResponseModel
    {
        public string Name { get; set; }

        public int MatchesPlayed { get; set; }

        public int Points { get; set; }

        public string LeagueName { get; set; }

        public int Position { get; set; }
    }
}
