﻿using System.Collections.Generic;

namespace FootballLeague.Data.Models.ResponseModels
{
    public class LeagueResponseModel
    {
        public string Name { get; set; }

        public IEnumerable<TeamResponseModel> Teams { get; set; }
    }
}
