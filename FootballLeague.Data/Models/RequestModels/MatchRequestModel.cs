﻿
namespace FootballLeague.Data.Models.RequestModels
{
    public class MatchRequestModel
    {
        public int Id { get; set; }

        public int HomeTeamId { get; set; }

        public int AwayTeamId { get; set; }

        public int WinnerId { get; set; }

        public int LeagueId { get; set; }
    }
}
