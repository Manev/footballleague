﻿
namespace FootballLeague.Data.Models.RequestModels
{
    public class TeamRequestModel
    {
        public string Name { get; set; }

        public int LeagueId { get; set; }
    }
}
