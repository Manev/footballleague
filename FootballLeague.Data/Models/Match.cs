﻿
namespace FootballLeague.Data.Models
{
    public class Match
    {
        public int Id { get; set; }

        public int HomeTeamId { get; set; }

        public int AwayTeamId { get; set; }

        public int LeagueId { get; set; }

        public string Result { get; set; }

        public bool IsPlayed { get; set; }

        public Team HomeTeam { get; set; }

        public Team AwayTeam { get; set; }

        public string WinnerName { get; set; }

        public League League { get; set; }
    }
}
