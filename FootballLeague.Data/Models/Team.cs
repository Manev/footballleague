﻿using System.Collections.Generic;

namespace FootballLeague.Data.Models
{
    public class Team
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int Points { get; set; }

        public int Rank { get; set; }

        public int LeagueId { get; set; }

        public League League { get; set; }

        public ICollection<Match> HomeMatches { get; set; }

        public ICollection<Match> AwayMatches { get; set; }

        public int MatchesPlayed
        {
            get
            {
               return HomeMatches.Count + AwayMatches.Count;
            }
        }
    }
}
