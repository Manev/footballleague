﻿using FootballLeague.Data.Models;

namespace FootballLeague.Repositories.MatchRepository
{
    public interface IMatchRepository : IGenericRepository<Match>
    {
    }
}
