﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace FootballLeague.Repositories
{
    public interface IGenericRepository<T> where T : class
    {
        Task<IEnumerable<T>> GetAllAsync();

        Task<T> GetAsync(int id);

        Task<T> CreateAsync(T entity);

        Task DeleteAsync(T entity);

        Task<T> UpdateAsync(T entity);
    }
}
