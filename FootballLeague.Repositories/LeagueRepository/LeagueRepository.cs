﻿using FootballLeague.Data.Context;
using FootballLeague.Data.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FootballLeague.Repositories.LeagueRepository
{
    public class LeagueRepository : ILeagueRepository
    {
        private readonly FootballLeagueDbContext _context;

        public LeagueRepository (FootballLeagueDbContext context)
        {
            _context = context;
        }

        public async Task<League> CreateAsync(League league)
        {
            _context.Leagues.Add(league);
            await _context.SaveChangesAsync();

            return league;
        }

        public async Task DeleteAsync(League league)
        {
            _context.Leagues.Remove(league);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<League>> GetAllAsync()
        {
            return await _context.Leagues.ToListAsync();
        }

        public async Task<League> GetAsync(int id)
        {
            return await _context.Leagues
                .Include(league => league.Teams)
                    .ThenInclude(team => team.HomeMatches)
                .Include(league => league.Teams)
                    .ThenInclude(team => team.AwayMatches)
                .Where(league => league.Id == id)
                .FirstOrDefaultAsync();
        }

        public async Task<League> UpdateAsync(League league)
        {
            _context.Entry(league).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return league;
        }
    }
}
