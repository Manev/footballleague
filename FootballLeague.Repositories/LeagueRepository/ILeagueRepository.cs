﻿using FootballLeague.Data.Models;

namespace FootballLeague.Repositories.LeagueRepository
{
    public interface ILeagueRepository : IGenericRepository<League>
    {
    }
}
