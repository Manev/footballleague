﻿using FootballLeague.Data.Models;

namespace FootballLeague.Repositories.TeamRepository
{
    public interface ITeamRepository : IGenericRepository<Team>
    {
    }
}
