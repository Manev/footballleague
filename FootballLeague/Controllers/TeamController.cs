﻿using FootballLeague.Data.Models.RequestModels;
using FootballLeague.Data.Models.ResponseModels;
using FootballLeague.Services.TeamService;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace FootballLeague.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamController : ControllerBase
    {
        private readonly ITeamService _teamService;

        public TeamController(ITeamService teamService)
        {
            _teamService = teamService;
        }

        /// <summary>
        /// Gets a team.
        /// </summary>
        [HttpGet("{id}")]
        public async Task<ActionResult<TeamResponseModel>> GetTeam(int id)
        {
            return Ok(await _teamService.GetAsync(id));
        }

        /// <summary>
        /// Creates a new team.
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST api/Team
        ///     {        
        ///       "name": "Liverpool"
        ///       "leagueId": "1"
        ///     }
        /// </remarks>
        [HttpPost]
        public async Task<ActionResult<TeamResponseModel>> CreateTeam(TeamRequestModel teamModel)
        {
            return Ok(await _teamService.CreateAsync(teamModel));
        }

        /// <summary>
        /// Deletes a team.
        /// </summary>
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteTeam(int id)
        {
            await _teamService.DeleteAsync(id);
            return Ok();
        }
    }
}
