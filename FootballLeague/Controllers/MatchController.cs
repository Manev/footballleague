﻿using FootballLeague.Data.Models.RequestModels;
using FootballLeague.Data.Models.ResponseModels;
using FootballLeague.Services.MatchService;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FootballLeague.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MatchController : ControllerBase
    {
        private readonly IMatchService _matchService;

        public MatchController(IMatchService matchService)
        {
            _matchService = matchService;
        }

        /// <summary>
        /// Creates a new Match.
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST api/Match
        ///     {        
        ///       "id": "1"
        ///       "homeTeamId": "1"  
        ///       "awayTeamId": "2"  
        ///       "winnerId": "1"  
        ///       "leagueId": "1"  
        ///     }
        /// </remarks>
        [HttpPost]
        public async Task<ActionResult<MatchResponseModel>> CreateMatch(MatchRequestModel model)
        {
            return Ok(await _matchService.CreateMatchAsync(model));
        }

        /// <summary>
        /// Gets all matches.
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MatchResponseModel>>> GetAllMatches()
        {
            var matches = await _matchService.GetAllMatchesAsync();
            return Ok(matches.ToList());
        }

        /// <summary>
        /// Gets a Match.
        /// </summary>
        [HttpGet("{id}")]
        public async Task<ActionResult<MatchResponseModel>> GetMatch(int id)
        {
            return Ok(await _matchService.GetMatchAsync(id));
        }

        /// <summary>
        /// Deletes a Match.
        /// </summary>
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteMatch(int id)
        {
            await _matchService.DeleteMatchAsync(id);
            return Ok();
        }

        /// <summary>
        /// Sets the Match to "played" and updates the stats of the teams involved.
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST api/Match
        ///     {        
        ///       "id": "1"
        ///       "homeTeamId": "1"  
        ///       "awayTeamId": "2"  
        ///       "winnerId": "1"  
        ///       "leagueId": "1"  
        ///     }
        /// </remarks>
        [HttpPut]
        public async Task<ActionResult<MatchResponseModel>> PlayMatch(MatchRequestModel matchModel)
        {
            return Ok(await _matchService.PlayMatchAsync(matchModel));
        }
    }
}
