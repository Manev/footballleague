﻿using FootballLeague.Data.Models.RequestModels;
using FootballLeague.Data.Models.ResponseModels;
using FootballLeague.Services.LeagueService;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace FootballLeague.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LeagueController : ControllerBase
    {
        private readonly ILeagueService _leagueService;

        public LeagueController(ILeagueService leagueService)
        {
            _leagueService = leagueService;
        }

        /// <summary>
        /// Creates a new league.
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST api/League
        ///     {        
        ///       "name": "Premier League"     
        ///     }
        /// </remarks>
        [HttpPost]
        public async Task<ActionResult<LeagueResponseModel>> CreateLeague(LeagueRequestModel leagueModel)
        {
            var newLeague = await _leagueService.CreateAsync(leagueModel);
            return Ok(CreatedAtAction(nameof(GetLeague), new { id = newLeague.Id }, newLeague));
        }

        /// <summary>
        /// Gets a League.
        /// </summary>
        [HttpGet("{id}")]
        public async Task<ActionResult<LeagueResponseModel>> GetLeague(int id)
        {
            return Ok(await _leagueService.GetAsync(id));
        }

        /// <summary>
        /// Deletes a league.
        /// </summary>
        [HttpDelete]
        public async Task<ActionResult> DeleteLeague(int id)
        {
            await _leagueService.DeleteAsync(id);
            return Ok();
        }
    }
}
