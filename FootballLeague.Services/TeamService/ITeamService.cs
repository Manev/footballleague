﻿using FootballLeague.Data.Models;
using FootballLeague.Data.Models.RequestModels;
using FootballLeague.Data.Models.ResponseModels;
using System.Threading.Tasks;

namespace FootballLeague.Services.TeamService
{
    public interface ITeamService
    {
        Task<TeamResponseModel> GetAsync(int id);

        Task<TeamResponseModel> CreateAsync(TeamRequestModel team);

        Task<Team> UpdateAsync(Team team);

        Task DeleteAsync(int id);
    }
}
