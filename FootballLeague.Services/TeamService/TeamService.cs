﻿using FootballLeague.Data.CustomExceptions;
using FootballLeague.Data.Models;
using FootballLeague.Data.Models.RequestModels;
using FootballLeague.Data.Models.ResponseModels;
using FootballLeague.Repositories.TeamRepository;
using FootballLeague.Services.LeagueService;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FootballLeague.Services.TeamService
{
    public class TeamService : ITeamService
    {
        private readonly ITeamRepository _teamRepository;
        private readonly ILeagueService _leagueService;

        public TeamService(ITeamRepository teamRepository, ILeagueService leagueService)
        {
            _teamRepository = teamRepository;
            _leagueService = leagueService;
        }

        public async Task<TeamResponseModel> CreateAsync(TeamRequestModel teamModel)
        {
            var teamExists = _teamRepository.GetAllAsync().Result.Where(l => l.Name == teamModel.Name).Any();
            var league = await _leagueService.GetAsync(teamModel.LeagueId);

            if (teamExists)
            {
                string message = ExceptionMessages.TeamAlreadyExists;
                throw new EntityAlreadyExistsException(message);
            }

            if (league == null)
            {
                string message = ExceptionMessages.LeagueNotFound;
                throw new EntityNotFoundException(message);
            }

            var team = new Team
            {
                Name = teamModel.Name,
                LeagueId = teamModel.LeagueId,
                HomeMatches = new List<Match>(),
                AwayMatches = new List<Match>(),
                Points = 0
            };

            await _teamRepository.CreateAsync(team);

            var teamResponseModel = MapToResponseModel(team);
            return teamResponseModel;
        }

        public async Task DeleteAsync(int id)
        {
            var teamToDelete = await _teamRepository.GetAsync(id);

            if (teamToDelete == null)
            {
                string message = ExceptionMessages.TeamDoesNotExist;
                throw new EntityNotFoundException(message);
            }

            await _teamRepository.DeleteAsync(teamToDelete);
        }

        public async Task<TeamResponseModel> GetAsync(int id)
        {
            var team = await _teamRepository.GetAsync(id);

            if (team == null)
            {
                string message = ExceptionMessages.TeamDoesNotExist;
                throw new EntityNotFoundException(message);
            }

            var teamResponseModel = MapToResponseModel(team);
            return teamResponseModel;
        }

        public async Task<Team> UpdateAsync(Team team)
        {
            return await _teamRepository.UpdateAsync(team);
        }

        private TeamResponseModel MapToResponseModel(Team team)
        {
            var teamModel = new TeamResponseModel
            {
                Name = team.Name,
                LeagueName = team.League.Name,
                MatchesPlayed = team.MatchesPlayed,
                Points = team.Points
            };

            return teamModel;
        }
    }
}
