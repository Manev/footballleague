﻿using FootballLeague.Data.CustomExceptions;
using FootballLeague.Data.Models;
using FootballLeague.Data.Models.RequestModels;
using FootballLeague.Data.Models.ResponseModels;
using FootballLeague.Repositories.MatchRepository;
using FootballLeague.Services.LeagueService;
using FootballLeague.Services.TeamService;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FootballLeague.Services.MatchService
{
    public class MatchService : IMatchService
    {
        private readonly ITeamService _teamService;
        private readonly ILeagueService _leagueService;
        private readonly IMatchRepository _matchRepository;

        public MatchService(ITeamService teamService, ILeagueService leagueService, IMatchRepository matchRepository)
        {
            _teamService = teamService;
            _leagueService = leagueService;
            _matchRepository = matchRepository;
        }


        public async Task<MatchResponseModel> CreateMatchAsync(MatchRequestModel matchModel)
        {
            var league = await _leagueService.GetAsync(matchModel.LeagueId);
            var homeTeam = await _teamService.GetAsync(matchModel.HomeTeamId);
            var awayTeam = await _teamService.GetAsync(matchModel.AwayTeamId);

            ValidateTeamAndLeague(league, homeTeam, awayTeam);

            var match = new Match()
            {
                HomeTeamId = matchModel.HomeTeamId,
                AwayTeamId = matchModel.AwayTeamId,
                IsPlayed = false,
                LeagueId = matchModel.LeagueId,
                WinnerName = matchModel.HomeTeamId == matchModel.WinnerId ? homeTeam.Name : awayTeam.Name
            };

            await _matchRepository.CreateAsync(match);

            return MapToResponseModel(match);
        }


        public async Task DeleteMatchAsync(int id)
        {
            var match = await _matchRepository.GetAsync(id);

            if (match.IsPlayed)
            {
                RevertTeamPoints(match);
            }

            await _teamService.UpdateAsync(match.HomeTeam);
            await _teamService.UpdateAsync(match.AwayTeam);
            await _matchRepository.DeleteAsync(match);
        }


        public async Task<IEnumerable<MatchResponseModel>> GetAllMatchesAsync()
        {
            var matches = await _matchRepository.GetAllAsync();

            var matchResponseModels = matches.Select(match => new MatchResponseModel()
            {
                LeagueName = match.League.Name,
                HomeTeamName = match.HomeTeam.Name,
                AwayTeamName = match.AwayTeam.Name,
                WinnerName = match.WinnerName
            });

            return matchResponseModels.ToList();
        }

        public async Task<MatchResponseModel> PlayMatchAsync(MatchRequestModel matchModel)
        {
            var match = await _matchRepository.GetAsync(matchModel.Id);

            if (match.IsPlayed)
            {
                string message = ExceptionMessages.MatchAlreadyPlayed;
                throw new MatchIsPlayedException(message);
            }

            AssignMatchStats(match, matchModel);

            await _teamService.UpdateAsync(match.HomeTeam);
            await _teamService.UpdateAsync(match.AwayTeam);
            await _matchRepository.UpdateAsync(match);

            return MapToResponseModel(match);
        }


        public async Task<MatchResponseModel> GetMatchAsync(int id)
        {
            var match = await _matchRepository.GetAsync(id);
                    
            if (match == null)
            {
                string message = ExceptionMessages.MatchNotFound;
                throw new EntityNotFoundException(message);
            }

            return MapToResponseModel(match);
        }

        private void ValidateTeamAndLeague(LeagueResponseModel league, TeamResponseModel homeTeam, TeamResponseModel awayTeam)
        {
            if (league == null || homeTeam == null || awayTeam == null)
            {
                string message = ExceptionMessages.GenericNotFoundMessage;
                throw new EntityNotFoundException(message);
            }

            if (league.Name != homeTeam.LeagueName || league.Name != awayTeam.LeagueName)
            {
                string message = ExceptionMessages.TeamNotInLeague;
                throw new EntityNotFoundException(message);
            }
        }

        private void RevertTeamPoints(Match match)
        {
            if (match.HomeTeam.Name == match.WinnerName)
            {
                match.HomeTeam.Points -= 3;
            }
            else if (match.AwayTeam.Name == match.WinnerName)
            {
                match.AwayTeam.Points -= 3;
            }
            else
            {
                match.HomeTeam.Points -= 1;
                match.AwayTeam.Points -= 1;
            }
        }

        private void AssignMatchStats(Match match, MatchRequestModel matchModel)
        {
            match.IsPlayed = true;

            if (match.HomeTeam.Id == matchModel.WinnerId)
            {
                match.HomeTeam.Points += 3;
            }
            else if (matchModel.AwayTeamId == matchModel.WinnerId)
            {
                match.AwayTeam.Points += 3;
            }
            else
            {
                match.HomeTeam.Points += 1;
                match.AwayTeam.Points += 1;
            }
        }

        private MatchResponseModel MapToResponseModel(Match match)
        {
            var matchModel = new MatchResponseModel
            {
                AwayTeamName = match.AwayTeam.Name,
                HomeTeamName = match.HomeTeam.Name,
                LeagueName = match.League.Name,
                WinnerName = match.WinnerName,
                Result = match.Result
            };

            return matchModel;
        }
    }
}