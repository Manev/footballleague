﻿using FootballLeague.Data.Models.RequestModels;
using FootballLeague.Data.Models.ResponseModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FootballLeague.Services.MatchService
{
    public interface IMatchService
    {
        Task<MatchResponseModel> CreateMatchAsync(MatchRequestModel matchModel);

        Task<MatchResponseModel> PlayMatchAsync(MatchRequestModel matchModel);

        Task<MatchResponseModel> GetMatchAsync(int id);

        Task<IEnumerable<MatchResponseModel>> GetAllMatchesAsync();

        Task DeleteMatchAsync(int id);
    }
}
