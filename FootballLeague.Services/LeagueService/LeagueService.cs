﻿using FootballLeague.Data.CustomExceptions;
using FootballLeague.Data.Models;
using FootballLeague.Data.Models.RequestModels;
using FootballLeague.Data.Models.ResponseModels;
using FootballLeague.Repositories.LeagueRepository;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FootballLeague.Services.LeagueService
{
    public class LeagueService : ILeagueService
    {
        private readonly ILeagueRepository _leagueRepository;

        public LeagueService(ILeagueRepository leagueRepository)
        {
            _leagueRepository = leagueRepository;
        }

        public async Task<League> CreateAsync(LeagueRequestModel leagueModel)
        {
            var leagueExists = _leagueRepository.GetAllAsync().Result.Where(l => l.Name == leagueModel.Name).Any();

            if (leagueExists)
            {
                string message = ExceptionMessages.LeagueAlreadyExists;
                throw new EntityAlreadyExistsException(message);
            }

            var league = new League
            {
                Name = leagueModel.Name
            };

            return await _leagueRepository.CreateAsync(league);
        }

        public async Task DeleteAsync(int id)
        {
            var leagueToDelete = await _leagueRepository.GetAsync(id);

            if (leagueToDelete == null)
            {
                string message = ExceptionMessages.LeagueNotFound;
                throw new EntityNotFoundException(message);
            }

            await _leagueRepository.DeleteAsync(leagueToDelete);
        }

        public async Task<LeagueResponseModel> GetAsync(int id)
        {
            var league = await _leagueRepository.GetAsync(id);

            if (league == null)
            {
                string message = ExceptionMessages.LeagueNotFound;
                throw new EntityNotFoundException(message);
            }

            var leagueModel = MapToResponseModel(league);

            AssignTablePositions(leagueModel.Teams);

            return leagueModel;
        }

        private void AssignTablePositions(IEnumerable<TeamResponseModel> teams)
        {
            int position = 0;

            foreach (var team in teams)
            {
                team.Position = ++position;
            }
        }

        private LeagueResponseModel MapToResponseModel(League league)
        {
            var leagueModel = new LeagueResponseModel
            {
                Name = league.Name,
                Teams = league.Teams.Select(team => new TeamResponseModel()
                {
                    Name = team.Name,
                    MatchesPlayed = team.MatchesPlayed,
                    Points = team.Points,
                    LeagueName = league.Name
                })
                    .OrderByDescending(team => team.Points)
                    .ThenByDescending(team => team.Name)
                    .ToList()
            };

            return leagueModel;
        }
    }
}
