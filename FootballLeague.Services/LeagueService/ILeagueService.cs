﻿using FootballLeague.Data.Models;
using FootballLeague.Data.Models.RequestModels;
using FootballLeague.Data.Models.ResponseModels;
using System.Threading.Tasks;

namespace FootballLeague.Services.LeagueService
{
    public interface ILeagueService 
    {
        Task<LeagueResponseModel> GetAsync(int id);

        Task<League> CreateAsync(LeagueRequestModel leagueModel);

        Task DeleteAsync(int id);
    }
}
